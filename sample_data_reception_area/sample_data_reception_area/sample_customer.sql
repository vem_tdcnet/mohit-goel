﻿CREATE TABLE [sample].[sample_customer]
(
	[CustomerId] INT NOT NULL PRIMARY KEY, 
    [FirstName] NCHAR(30) NULL, 
    [LastName] NCHAR(30) NULL, 
    [MixName] NCHAR(70) NOT NULL, 
    [AddressId] INT NOT NULL, 
    [ContactId] INT NOT NULL, 
    [CustomerType] NCHAR(20) NOT NULL, 
    [OrganizationOfSP] NCHAR(10) NULL,
    test varchar(50) 
)

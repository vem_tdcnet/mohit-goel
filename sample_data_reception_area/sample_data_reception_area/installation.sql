﻿CREATE TABLE [sample].[installation]
(
	[InstallationId] INT NOT NULL PRIMARY KEY, 
    [InstallationStatus] NCHAR(10) NULL
)

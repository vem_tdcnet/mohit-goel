const Sequelize = require('sequelize')
const dotenv = require('dotenv')
dotenv.config()

const sequelize = new Sequelize('Sales', 'sa', 'yourStrong(!)Password', {
  host: process.env.MSSQL_HOST,
  dialect: 'mssql',
})

sequelize
  .authenticate()
  .then(() => console.log('Sequelize properly connected'))
  .catch(console.error)
